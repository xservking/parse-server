#!/usr/bin/env bash

FILENAME="docker-compose.yml"
TEMPLATENAME="docker-compose.template.yml"

##### Check arguments
check()
{
	if [[ "$appname" == "" ]]; then
		echo "Wrong value appname"
		exit 1
	fi
	if [[ "$host" == "" ]]; then
                echo "Wrong value host"
                exit 1
        fi
        if [[ "$parse_port" == "" ]]; then
                echo "Wrong value parse_port"
                exit 1
        fi
        if [[ "$dashboard_port" == "" ]]; then
                echo "Wrong value dashboard_port"
                exit 1
        fi
        if [[ "$login" == "" ]]; then
                echo "Wrong value username"
                exit 1
        fi
        if [[ "$password" == "" ]]; then
                echo "Wrong value password"
                exit 1
        fi

}

##### Create Instance
instance()
{
	check
	mkdir -p $appname
	cp $TEMPLATENAME $appname/$FILENAME
	cd $appname

	sed -i "s/<host>/$host/g; s/<port_parse>/$parse_port/g; s/<port_dashboard>/$dashboard_port/g; s/<login>/$login/g; s/<password>/$password/g; s/<appname>/$appname/g" $FILENAME

	docker-compose -f $FILENAME up -d

	cd ..
	echo Instance was created!
}

##### Usage
usage()
{
	echo "Usage: createInstance -n -h -u -P --parse_port --dashboard_port | -i

Where:
	-n | --appname		application name
	-h | --host		ip-address or hostname
        --parse_port		parse-server port
	--dashboard_port	parse-dashboard port
	-u | --user		dashboard username
	-P | --password		dashboard password
	-i | --interactive	interactive mode
	-h | --help		show this message
"
}


##### Main

interactive=
appname=
host=
parse_port=1337
dashboard_port=4040
login=admin
password=

while [ "$1" != "" ]; do
    case $1 in
        -n | --appname )        shift
                                appname=$1
                                ;;
	-h | --host )		shift
				host=$1
				;;
	--parse_port )		shift
				parse_port=$1
				;;
	--dashboard_port )	shift
				dashboard_port=$1
				;;
	-u | --user )		shift
				login=$1
				;;
	-P | --password )	shift
				password=$1
				;;
        -i | --interactive )    interactive=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


if [ "$interactive" = "1" ]; then
	## Ask information
	read -p "Enter application name: " appname
	read -p "Enter host: " host
	read -p "Enter parse-server port [default=1337]: " parse_port
	read -p "Enter parse-dashboard port [default=4040]: " dashboard_port
	read -p "Enter login [default=admin]: " login
	read -e -s -p "Enter password: " password
	echo

	# set default
	login=${login:-admin}
	parse_port=${parse_port:-1337}
	dashboard_port=${dashboard_port:-4040}
fi

instance
