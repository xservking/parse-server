# Dependencies
* [docker](https://docs.docker.com/install/)
* [docker-compose](https://docs.docker.com/compose/install/)

# How to use
Запустить скрипт createInstance  
Параметры:
* -n | --appname - название приложения
* -h | --host   - ip-адрес либо hostname для parse-server и parse-dashboard
* --parse_port  - порт для parse-сервера
* --dashboard_port - порт для parse-dashboard
* -u | --user   - username для dashboard
* -P | --password   - password для dashboard

Либо вызвать ``` createInstance -i ``` для ввода данных в интерактивном режиме


Для отключения конкретного инстанса необходимо в его каталоге выполнить:  
``` docker-compose down ```
